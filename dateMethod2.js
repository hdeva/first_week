/*
A new planet was discovered. This is called Planet Alpha. 
Scientists have made a new calendar for this planet and it has only 9 months each and the number of days in each month is below.

January - 30
February - 20
March - 30
April - 25
May - 30
June - 25
July - 30
August - 30
September - 25

Date 02/03/04 means, in the year 04, the 2nd of March. Please note that 26/06/22 is an invalid date, because June has only 25 days. 

Write a function to calculate the difference between two date strings for this planet: "19/02/05" and "22/06/86"
*/

// function for validation
/**
 * validates the date format according to new calendar
 * @param {*} str 
 * @returns true or false
 * 
 * checks the given input whether it is valid or not,
 * checks for null,typeof and date format using regEx
 */
function validate_date_string(str) {
  // check for empty input
  if (str === null) {
    return false;
  }

  // splitting the date into day, month and year, and checking its length
  s = str.split("/");
  if (s.length !== 3) {
    return false;
  }

  //date format using regular expression
  let dateformat = /^(0?[1-9]|[1-2][0-9]|30)[\/](0?[1-9])[\/]([0-9]*)$/;
  if (dateformat.test(str)) {
    let s = str.split("/");
    day = parseInt(s[0]); 				//day
    month = parseInt(s[1]);			  //month
    year = parseInt(s[2]); 				//year
    var months = [30, 20, 30, 25, 30, 25, 30, 30, 25];

    // checking for no. of days for corresponding months
    if (day <= months[month - 1]) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

// calling validate_date_string function
var y = validate_date_string("25/02/99");
console.log(y);

//function to check which date is greater
/**
 * finds the greater date between two dates
 * @param {*} date1 
 * @param {*} date2 
 * @returns -1 or 0 or 1
 * 
 * splits the date into three parts as day,month and year,
 * first compares the years and then month and then day ,
 * returns -1 if date2 is greater,returns 1 if date1 is greater and returns 0 if both dates are same
 *  
 */
function greaterthan(date1, date2) {
  if (validate_date_string(date1) && validate_date_string(date2)) {
    var s1 = date1.split("/");
    var s2 = date2.split("/");
    day1 = parseInt(s1[0]); 				//day of 1st date
    month1 = parseInt(s1[1]); 			//month of 1st date
    year1 = parseInt(s1[2]);			 //year of 1st date
    day2 = parseInt(s2[0]);				 //day of 2nd date
    month2 = parseInt(s2[1]);			 //month of 2nd date
    year2 = parseInt(s2[2]); 				//year of 2nd date

    // checking year
    if (year1 > year2) {
      return 1;
    }

    // if year is equal, checking for month
    else if (year1 === year2 && month1 > month2) {
      return 1;
    }

    // if year and month are equal, checking for day
    else if (year1 === year2 && month1 === month2) {
      if (day1 > day2) {
        return 1;
      } else if (day1 === day2) {
        return 0;
      }
    } else return -1;
  } else {
    return "Invalid date format";
  }
}


// calling greaterthan function
var p = greaterthan("22/9/2022", "21/9/2022");
console.log(p);


/**
 * gives the next date for given date
 * @param {*} str 
 * @returns date for successive day
 * 
 * converts date from string to 3 sub parts in number format,
 * checks conditions for all no.of days with their respective months
 */
function get_next(str) {
  let s = str.split("/");
  // s=['20','09','10']
  //converting into numbers
  day = parseInt(s[0]); 			 //day
  month = parseInt(s[1]);			 //month
  year = parseInt(s[2]); 			//year
  if (day < 20) {
    day += 1;
  }

  // condition for febraury 20
  else if (day === 20 && month === 2) {
    day = 1;
    month += 1;
  }

  // condition for days less than 25
  else if (day < 25) {
    day += 1;
  }

  // condition for day 25 in april and june
  else if (day === 25 && month in [4, 6]) {
    day = 1;
    month += 1;
  }

  //condition for day 25 in september
  else if (day === 25 && month === 9) {
    day = month = 1;
    year += 1;
  }

  // condition for days less than 30
  else if (day <= 29) day += 1;
  // condition for day 30
  else if (day === 30) {
    day = 1;
    month += 1;
  }

  // converting into date format
  if (day < 10) {
    return "0" + String(day) + "/0" + String(month) + "/" + String(year);
  }
  return String(day) + "/0" + String(month) + "/" + String(year);
}

// calling get_next function
var q = get_next("29/09/2021");
console.log(q);


/**
 * counts no.of days difference between two dates
 * @param {*} str1 
 * @param {*} str2 
 * @returns no.of days
 * validates the two dates, checks the greater date , 
 * counts no.of days from 1st date by incrementing it using get_next function
 */
function get_difference(str1, str2) {
  // checking for validation
  if ((validate_date_string(str1) && validate_date_string(str2)) === false) {
    return "invalid date";
  }

  // checking for greater number
  if (greaterthan(str1, str2) === 1) {
    [str1, str2] = [str2, str1];
  }

  // checking for same date
  if (greaterthan(str1, str2) === 0) {
    return 0;
  }

  // counting no.of days from 1st date by incrementing it using get_next function
  count = 0;
  while (str1 !== str2) {
    count += 1;
    str1 = get_next(str1);
  }

  // returning the count value
  return count;
}


// calling get_difference function
var x = get_difference("25/09/22", "01/01/23");
console.log(x);



function get_day(str){
	if(validate_date_string(str) === false){
		return "Invalid date";
	}
	let date=str.split("/");
	day=parseInt(date[0]);
	mon=parseInt(date[1]);
	year==parseInt(date[2]);
	let week=["Tuesday1","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday1","Monday1"];
	var months = [30, 20, 30, 25, 30, 25, 30, 30, 25];
	var monthDays=0;
	for(let i = 0; i < mon-1; i++){
		 monthDays = monthDays + months[i];
	}
	
	var totalDays = (245*(year)) + day + monthDays;
	
	var weekCount = totalDays % week.length;
	console.log(week[weekCount])

}
var z=get_day("01/01/00");
console.log(z);