const EMPTY_GRID_CHAR = " ";
const FLY_IMAGES = [".", ".", "-", "*", "+", "+", "x", "x", "X", "X"]
const REFRESH_HZ = 24

/**
 * This Class represents a grid to b printed on to the screen.
 * The grid represents a fly trapped inside a 2-d box.
 */
class Grid {
	
	/**
	 * Constructor to initialize the grid parametrically
	 * @param {Number} height    the height of the grid 
	 * @param {Number} width     the width of the grid
	 */
	constructor(height, width) {

		// initializing the dimensions of the grid
		this.height = height;
		this.width = width;

		this.plane = []
		// initializing the grid
		for (var row_index = 0; row_index < height; row_index++) {
			var row = [];
			for (var col_index = 0; col_index < width; col_index++) {
				row.push(EMPTY_GRID_CHAR);
			}
			this.plane.push(row);
		}

		// initialize the fly
		this.fly_x = Math.floor(Math.random() * width)
		this.fly_y = Math.floor(Math.random() * height);
	}

	/**
	 * This is a helper method to print the grid to the screen.
	 */
	print_grid(fly_index) {

		console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		console.log("---------------------------------------------->");
		for (var row_index = 0; row_index < this.height; row_index++) {
			var output_string = "";
			for (var col_index = 0; col_index < this.width; col_index++) {
				var char_to_append = EMPTY_GRID_CHAR
				if (this.fly_x == row_index && this.fly_y == col_index){
					char_to_append = FLY_IMAGES[fly_index]
				}
				output_string += char_to_append
			}
			console.log("|" + output_string + "|");
		}
		console.log("<----------------------------------------------")
	}

	/**
	 * Internal helper method to calculate a delta of movement of the fly
	 * @returns a number between
	 */
	_get_random_delta() {
		return Math.floor(Math.random() * 3) - 1;
	}

	update_fly_position() {
		var new_fly_x = this.fly_x + this._get_random_delta();
		var new_fly_y = this.fly_y + this._get_random_delta();

		if (new_fly_x < 0 || new_fly_x >= this.width) {
			new_fly_x = this.fly_x;
		}
		if (new_fly_y < 0 || new_fly_y >= this.height) {
			new_fly_y = this.fly_y;
		}

		this.fly_x = new_fly_x
		this.fly_y = new_fly_y
	}
}

var g = new Grid(10, 10)
var count = 0
while (true) {	// starting an infinite loop
	// sleep for a while
	var milliseconds = 1000/REFRESH_HZ
	var currentTime = new Date().getTime();
	while (currentTime + milliseconds >= new Date().getTime()) {
	}

	// print the screen
	g.print_grid(count%FLY_IMAGES.length)
	g.update_fly_position()
	count++
}