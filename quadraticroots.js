/**
 * finds the roots of a quadratic function
 *
 * @param {*} a
 * @param {*} b
 * @param {*} c
 * @returns value of x
 *
 */

function quadraticRoots(a, b, c) {
  var a, b, c;
  if (a === 0) {
    if (b === 0) {
      return 0;
    } else {
      return (x = -c / b);
    }
  } else {
    D = Math.pow(b, 2) - 4 * a * c;
    if (D < 0) {
      rx1 = (-b / 2 * a);
      ix1 = Math.sqrt(-D) / 2 * a;
      rx2 = (-b / 2 * a);
      ix2 = Math.sqrt(-D) / 2 * a;
      return "Complex roots";
    } else {
      if (D === 0) {
        return (x = (-b / 2) * a);
      } else {
        x1 = ((-b - Math.sqrt(D)) / 2 * a);
        x2 = ((-b + Math.sqrt(D)) / 2 * a);
        console.log(x1, x2);
      }
    }
  }
}

var v = quadraticRoots(2, 5, 1);
console.log(v);
