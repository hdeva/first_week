var check_program = 7;

switch (check_program) {
  case 1:
    const first_program = require("./dateMethod1");

    break;

  case 2:
    const second_program = require("./dateMethod2");

    break;

  case 3:
    const third_program = require("./quadraticroots");
    break;

  case 4:
    const fourth_program = require("./matrix2d");

    break;

  case 5:
    const fifth_program = require("./matrixMul3d");

    break;

  case 6:
    const sixth_program = require("./grid");
    break;
	case 7:
    const seventh_program = require("./variables");
    break;
}
