/*
A new planet was discovered. This is called Planet Alpha. 
Scientists have made a new calendar for this planet and it has only 9 months each and the number of days in each month is below.

January - 30
February - 20
March - 30
April - 25
May - 30
June - 25
July - 30
August - 30
September - 25

Date 02/03/04 means, in the year 04, the 2nd of March. Please note that 26/06/22 is an invalid date, because June has only 25 days. 

Write a function to calculate the difference between two date strings for this planet: "19/02/05" and "22/06/86"
*/ 

var date1="25/09/06";
var date2="01/01/86";
var dt1=date1.split("/");
var dt2=date2.split("/");
var months=[30,20,30,25,30,25,30,30,25];
var totalDaysInYear = 0;
for (let i = 0; i < months.length; i++) {
    totalDaysInYear += months[i];
}
var day1=parseInt(dt1[0]);
var day2=parseInt(dt2[0]);
var mon1=parseInt(dt1[1]);
var mon2=parseInt(dt2[1]);
var yr1=parseInt(dt1[2]);
var yr2=parseInt(dt2[2]);

var yearsDiff,monthsDiff,daysDiff;
if(yr2>yr1){
    yearsDiff=yr2-yr1-1;
    daysInYear=yearsDiff*totalDaysInYear;
}
else if(yr2=yr1){
    daysInYear=0;
}
else{
    yearsDiff=yr1-yr2-1;
    daysInYear=yearsDiff*totalDaysInYear;
}

let yr1days = 0;
let yr2days = 0;
for(let j=0; j<mon1-1; j++){
    yr1days += months[j];
}
let days1InDays=totalDaysInYear-(yr1days+day1);

for(let k=0; k<mon2-1; k++){
    yr2days += months[k];
}
let days2InDays=yr2days+day2;

let finalDays=daysInYear+days1InDays+days2InDays;


console.log(finalDays);