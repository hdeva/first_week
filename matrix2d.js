function matrix2Mul(mat1, mat2, res) {
  var mat1 = [
    [1, 2],
    [3, 4],
  ];
  var mat2 = [
    [4, 5],
    [6, 7],
  ];
  var matrix1_row_length = mat1.length;
  var matrix2_column_length = mat2[0].length;

  if (mat1[0].length !== mat2.length) {
    return "Invalid matrices,  length of row of 1st matrix must be equal to length of column in 2nd matrix ";
  } else {
    var row, col, k;
    var res = [];
    for (row = 0; row < matrix1_row_length; row++) {
      res[row] = [];
      for (col = 0; col < matrix2_column_length; col++) {
        sum = 0;
        for (k = 0; k < mat2.length; k++) {
          sum += mat1[row][k] * mat2[k][col];
        }
        res[row][col] = sum;
      }
    }
    return res;
  }
}
var a = matrix2Mul();
console.log(a);
